import React from "react"
import {FruitProvider} from "./FruitContext"
import {FruitUpdateProvider} from "./FruitUpdateContext"
import FruitList from "./FruitList"

const Fruit = () =>{
  return(
    <FruitProvider>
      <FruitUpdateProvider>
        <FruitList/>
      </FruitUpdateProvider>
    </FruitProvider>
  )
}

export default Fruit
