import React, {useContext} from "react"
import {FruitContext} from "./FruitContext"
import {FruitUpdateContext} from "./FruitUpdateContext"
import axios from "axios"
import FruitForm from "./FruitForm"

const FruitList = () =>{
  const [fruits, setFruits] = useContext(FruitContext)
  const [fruit, setFruit] = useContext(FruitUpdateContext)

  const handleDelete = (event) => {
    let index = event.target.value
		axios.delete(`http://backendexample.sanbercloud.com/api/fruits/` + index)
	    .then(res => {
	      getData()
	    })
  }

  const handleUpdate = (event) => {
    setFruit(fruits[event.target.value])
  }

  const getData = () => {
    axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        setFruits(res.data)
      })
	}

  return(
    <section id="section-table">
      <h1>Daftar Harga Buah</h1>
      <table id="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Weight</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <FruitForm />
          {fruits.map((el, i) => {
            return (
              <tr className={"row-fruit"} key={i}>
                <td>{el.name}</td>
                <td className="text-center">{el.price}</td>
                <td className="text-center">{el.weight / 1000} kg</td>
                <td className="text-center">
                  <button className="button-info" onClick={handleUpdate} value={i}>Update</button>
                  <button className="button-danger" onClick={handleDelete} value={el.id}>Delete</button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </section>
  )
}

export default FruitList
