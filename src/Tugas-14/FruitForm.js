import React, {useContext} from "react"
import {FruitContext} from "./FruitContext"
import {FruitUpdateContext} from "./FruitUpdateContext"
import axios from "axios"

const FruitForm = () => {
  const [fruits, setFruits] = useContext(FruitContext)
  const [fruit, setFruit] = useContext(FruitUpdateContext)

  const clear = () => {
    setFruit({id: null, name: "", weight: 0, price: 0})
  }

  const handleSubmit = (event) =>{
    event.preventDefault()
    if (fruit.id === null){
      handleAdd()
    } else {
      handleUpdate()
    }
  }

  const handleAdd = () => {
    axios.post(`http://backendexample.sanbercloud.com/api/fruits`,
				{ name: fruit.name, weight: fruit.weight, price: fruit.price })
	    .then(res => {
	      getData()
        clear()
	    })
  }

  const handleUpdate = () => {
    axios.put(`http://backendexample.sanbercloud.com/api/fruits/` + fruit.id,
				{ name: fruit.name, weight: fruit.weight, price: parseInt(fruit.price) })
	    .then(res => {
	      getData()
        clear()
	    })
  }

  const getData = () => {
    axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
    .then(res => {
      setFruits(res.data)
    })
	}

  const handleChangeName = (event) =>{
    setFruit({...fruit, name: event.target.value})
  }

  const handleChangePrice = (event) =>{
    setFruit({...fruit, price: event.target.value})
  }

  const handleChangeWeight = (event) =>{
    setFruit({...fruit, weight: event.target.value})
  }

  return(
    <tr>
      <td className="text-center"><input type="text" placeholder="name..." value={fruit.name} onChange={handleChangeName} /></td>
      <td className="text-center"><input type="number" value={fruit.price} onChange={handleChangePrice} /></td>
      <td className="text-center"><input type="number" value={fruit.weight} onChange={handleChangeWeight} /></td>
      <td className="text-center"><button className="button-success" onClick={handleSubmit}>Submit</button>
          <button className="button-warning" onClick={clear}>Clear</button></td>
    </tr>
  )

}

export default FruitForm
