import React, {useState, useEffect, createContext} from 'react'
import axios from 'axios'

export const FruitContext = createContext()

export const FruitProvider = props => {
  const [fruits, setFruits] = useState([])

  useEffect( () => {
		getData()
  }, [])

	const getData = () => {
    axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
    .then(res => {
      setFruits(res.data)
    })
	}

  return (
    <FruitContext.Provider value={[fruits, setFruits]}>
      {props.children}
    </FruitContext.Provider>
  )
}
