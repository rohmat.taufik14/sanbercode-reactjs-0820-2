import React, {useState, createContext} from 'react'
import axios from 'axios'

export const FruitUpdateContext = createContext()

export const FruitUpdateProvider = props => {
  const [fruit, setFruit] = useState({id:null, name:"", price:0, weight:0})

  return (
    <FruitUpdateContext.Provider value={[fruit, setFruit]}>
      {props.children}
    </FruitUpdateContext.Provider>
  )
}
