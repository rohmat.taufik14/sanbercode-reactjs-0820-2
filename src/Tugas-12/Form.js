import React from 'react'

const Modal = ({
    handleClose,
    handleSave,
    handleChangeNama,
    handleChangeBerat,
    handleChangeHarga,
    show,
    nama,
    berat,
    harga
}) => {
    const showHideClassName = show ? 'modal display-block' : 'modal display-none';

    return (
      <div id="formModal" className={showHideClassName}>
        <div className="modal-content">
          <div className="modal-header">
            <span onClick={handleClose} className="close"> &times; </span>
            <h2> Add data </h2>
          </div>
          <div className="modal-body">
            <table>
              <tbody>
                <tr>
                  <td> Name </td>
                  <td> <input type="text"
                          onChange={handleChangeNama}
                          defaultValue={nama}/>
                  </td>
                </tr>
                <tr>
                  <td> Harga </td>
                  <td> <input type="text"
                          onChange={handleChangeHarga}
                          defaultValue={harga}/>
                  </td>
                </tr>
                <tr>
                  <td> Berat(g) </td>
                  <td> < input type="text"
                          onChange={handleChangeBerat}
                          defaultValue={berat}/>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="modal-footer">
            <button onClick={handleSave}> save </button>
          </div>
        </div>
      </div>
    );
};

class Form extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false,
            nama: props.fruit.nama,
            harga: props.fruit.harga,
            berat: props.fruit.berat
        }
    }

    showModal = () => {
        this.setState({
            show: true
        });
    }

    hideModal = () => {
        this.setState({
            show: false
        });
    }

    submit = () => {
        this.setState({
            show: false
        });
        let newFruit = {
            nama: this.state.nama,
            harga: this.state.harga,
            berat: this.state.berat
        }
        this.props.add(newFruit)
    }

    handleChangeNama = (event) => {
        this.setState({
            nama: event.target.value
        });
    }

    handleChangeBerat = (event) => {
        this.setState({
            berat: event.target.value
        });
    }

    handleChangeHarga = (event) => {
        this.setState({
            harga: event.target.value
        });
    }

    render() {
        return (
          <div>
            <button type='button' onClick={ this.showModal }> Tambah < /button>
            <Modal
              show={ this.state.show }
              handleClose={ this.hideModal }
              handleSave={ this.submit }
              handleChangeNama={ this.handleChangeNama }
              handleChangeBerat={ this.handleChangeBerat }
              handleChangeHarga={ this.handleChangeHarga }
              nama={ this.state.nama }
              berat={ this.state.berat }
              harga={ this.state.harga } />
          </div>
        )
    }
}

export default Form
