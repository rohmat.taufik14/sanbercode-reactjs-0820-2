import React from 'react'
import Update from './Update'

class ButtonDelete extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      index: props.index
    }
  }

  handleDelete = () => {
    this.props.handleDelete(this.state.index)
  }

  render(){
    return(
      <button onClick={this.handleDelete}>Delete</button>
    )
  }
}

class Table extends React.Component {
  constructor(props){
    super(props)
    this.handleUpdate = this.handleUpdate.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
  }

  handleUpdate(index, updateData) {
    this.props.update(index, updateData)
  }

  handleDelete(index) {
    this.props.delete(index)
  }

  render() {
    return (
      <table id="table">
        <thead>
  				<tr>
  					<th>Nama</th>
  					<th>Harga</th>
  					<th>Berat</th>
            <th>Action</th>
  				</tr>
        </thead>
        <tbody>
  				{(this.props.fruits).map((el, i) => {
  					return (
  						<tr className={"row-fruit"} key={i}>
  							<td>{el.nama}</td>
  							<td>{el.harga}</td>
  							<td>{el.berat / 1000} kg</td>
                <td>
                  <Update
                    handleUpdate={this.handleUpdate}
                    index={i}
                    fruit={el}>Update</Update>
                  <ButtonDelete handleDelete={this.handleDelete} index={i}/>
                </td>
  						</tr>
  					)
  				})}
        </tbody>
      </table>
    );
  }
}


export default Table
