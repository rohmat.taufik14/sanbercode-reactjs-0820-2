import React from 'react'
import Form from './Form'
import Table from './Table'

class Title extends React.Component {
	render() {
		return <h1>{this.props.title}</h1>;
	}
}

class Fruit extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      fruits: [
        {nama: "Semangka", harga: 10000, berat: 1000},
        {nama: "Anggur", harga: 40000, berat: 500},
        {nama: "Strawberry", harga: 30000, berat: 400},
        {nama: "Jeruk", harga: 30000, berat: 1000},
        {nama: "Mangga", harga: 30000, berat: 500}
      ],
      defaultData: {
        nama: "",
        harga: 0,
        berat: 0
      }
    }
    this.add = this.add.bind(this);
    this.update = this.update.bind(this);
    this.delete = this.delete.bind(this);
  }

  add(fruit){
    this.setState({ fruits: [...this.state.fruits, fruit]})
  }

  update(index, updateData){
    let fruits = this.state.fruits
    fruits[index] = updateData
    this.setState({ fruits: fruits})
  }

  delete(index){
    var fruits = [...this.state.fruits]
    fruits.splice(index, 1);
    this.setState({fruits: fruits});
  }

  render() {
  	return (
      <section id="section-table">
  			<Title title="Daftar Harga Buah" />
        <Form add={this.add} fruit={this.state.defaultData} />
        <Table
          update={this.update}
          delete={this.delete}
          fruits={this.state.fruits} />
      </section>
    )
  }
}


export default Fruit
