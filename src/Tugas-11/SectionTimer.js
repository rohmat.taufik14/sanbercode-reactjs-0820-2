import React from 'react'

class Timer extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      counter: 100,
      timer: Date.now()
    }
  }

  componentDidMount(){
    this.timer = setInterval(
      () => this.tick(),
      1000
    )
  }

  componentWillUnmount(){
    clearInterval(this.timer)
  }

  tick(){
    if (this.state.counter <= 1) {
      this.props.removeTimer();
    } else {
      this.setState({
        counter: this.state.counter - 1,
        timer: Date.now()
      })
    }
  }

  render(){
    return(
      <div id="div-timer">
        <span id="time">Sekarang jam : { new Intl.DateTimeFormat('en-US', {hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(this.state.timer) }</span>
        <span id="counter">Hitung mundur : { this.state.counter }</span>
      </div>
    )
  }
}


class SectionTimer extends React.Component {
  constructor(props){
    super(props)
    this.state = {show: true};
    this.removeTimer = this.removeTimer.bind(this);
  }

  removeTimer(){
    this.setState({show: false});
  }

  render(){
    let timer;
    if (this.state.show) {
      timer = <Timer removeTimer={this.removeTimer} />;
    }
    return(
      <section id="section-timer">
        <div>{timer}</div>
      </section>
    )
  }
}

export default SectionTimer
