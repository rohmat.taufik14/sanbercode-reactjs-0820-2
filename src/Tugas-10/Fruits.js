import React from 'react';

let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]

class Title extends React.Component {
	render() {
		return <h1>{this.props.title}</h1>;
	}
}

class Table extends React.Component {
  render() {
    return (
      <table id="table">
				<tr>
					<th>Nama</th>
					<th>Harga</th>
					<th>Berat</th>
				</tr>
				{dataHargaBuah.map(el=> {
					return (
						<tr>
							<td>{el.nama}</td>
							<td>{el.harga}</td>
							<td>{el.berat / 1000} kg</td>
						</tr>
					)
				})}
      </table>
    );
  }
}

class Fruits extends React.Component {
  render() {
  	return (
      <section id="section-table">
  			<Title title="Daftar Harga Buah" />
        <Table />
      </section>
    )
  }
}


export default Fruits
