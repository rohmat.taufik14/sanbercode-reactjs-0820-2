import React from 'react';

let dataBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]

class Title extends React.Component {
	render() {
		return <h1>{this.props.title}</h1>;
	}
}

class Table extends React.Component {
  render() {
    return (
      <table>
        <tr width="100%">
          <td class="label" width="50%"><strong>Nama Pelanggan</strong></td>
          <td class="input"><input type="text" /></td>
        </tr>
        <tr>
          <td className="label"><strong>Daftar Item</strong></td>
          <td className="input">
            {dataBuah.map(el=> {
              return (
                <div>
                  <input type="checkbox" name="item" value="{el.nama}" />
                  <span>{el.nama}</span>
                </div>
              )
            })}
          </td>
        </tr>
      </table>
    );
  }
}

class Form extends React.Component {
  render() {
		return (
      <table id="form">
        <div class="form-group">
          <Table />
        </div>
        <input type="submit" value="Kirim" id="button-submit" />
      </table>
    );
	}
}

class FormFruit extends React.Component {
  render() {
  	return (
      <section id="section-form">
  			<Title title="Form Pembelian Buah" />
        <Form />
      </section>
    )
  }
}


export default FormFruit
