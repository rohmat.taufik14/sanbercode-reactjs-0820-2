import React from "react";
import FormFruit from '../Tugas-9/FormFruit'
import Fruits from '../Tugas-10/Fruits'
import SectionTimer from '../Tugas-11/SectionTimer'
import FruitTugas12 from '../Tugas-12/Fruit'
import FruitTugas13 from '../Tugas-13/Fruit'
import FruitTugas14 from '../Tugas-14/Fruit'
import { Switch, Route } from "react-router";

const Routes = () => {

  return (
    <Switch>
      <Route exact path="/">
        <FruitTugas14 />
      </Route>
      <Route path="/tugas13">
        <FruitTugas13 />
      </Route>
      <Route exact path="/tugas12">
        <FruitTugas12 />
      </Route>
      <Route exact path="/tugas11">
        <SectionTimer />
      </Route>
      <Route path="/tugas10">
        <Fruits />
      </Route>
      <Route exact path="/tugas9">
        <FormFruit />
      </Route>
    </Switch>
  );
};

export default Routes;
