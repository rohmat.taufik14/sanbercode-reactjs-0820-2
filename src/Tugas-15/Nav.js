import React, {useContext} from "react";
import {Link} from "react-router-dom";
import {NavColorContext} from "./NavColorContext"
import {NavColorProvider} from "./NavColorContext"

const NavContent = () => {
  const [navColor, setNavColor] = useContext(NavColorContext)

  const changeColor = () => {
    if (navColor === "default") {
      setNavColor("light")
    } else {
      setNavColor("default")
    }
  }

  return (
    <ul className={"nav nav-"+navColor}>
      <li>
        <Link to="/">Tugas 14</Link>
      </li>
      <li>
        <Link to="/tugas13">Tugas 13</Link>
      </li>
      <li>
        <Link to="/tugas12">Tugas 12</Link>
      </li>
      <li>
        <Link to="/tugas11">Tugas 11</Link>
      </li>
      <li>
        <Link to="/tugas10">Tugas 10</Link>
      </li>
      <li>
        <Link to="/tugas9">Tugas 9</Link>
      </li>
      <li className="change-color-button">
        Change Theme : <button onClick={changeColor}>{navColor === 'default' ? 'light' : 'default'}</button>
      </li>
    </ul>
  )
}

const Nav = () => {
  return (
    <NavColorProvider>
      <NavContent />
    </NavColorProvider>
  )
}

export default Nav
