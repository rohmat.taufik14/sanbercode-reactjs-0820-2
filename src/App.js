import React, { Component } from 'react'
import './App.css'
import { BrowserRouter as Router } from "react-router-dom";
import Nav from './Tugas-15/Nav'
import Routes from './Tugas-15/Routes'

class App extends Component {
  render() {
    return (
        <div className="App">
          <Router>
            <Nav />
            <Routes />
          </Router>
        </div>
    )
  }
}

export default App
