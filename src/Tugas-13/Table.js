import React from 'react'
import Update from './Update'

const Table = ({updateFruit, deleteFruit, fruits}) => {

  const handleUpdate = (index, updateData) => {
    updateFruit(index, updateData)
  }

  const handleDelete = (event) => {
    deleteFruit(event.target.value)
  }

  return (
    <table id="table">
      <thead>
				<tr>
					<th>Nama</th>
					<th>Harga</th>
					<th>Berat</th>
          <th>Action</th>
				</tr>
      </thead>
      <tbody>
				{fruits.map((el, i) => {
					return (
						<tr className={"row-fruit"} key={i}>
							<td>{el.nama}</td>
							<td>{el.harga}</td>
							<td>{el.berat / 1000} kg</td>
              <td>
                <Update
                  handleUpdate={handleUpdate}
                  index={el.id}
                  fruit={el}>Update</Update>
                <button onClick={handleDelete} value={el.id}>Delete</button>
              </td>
						</tr>
					)
				})}
      </tbody>
    </table>
  );
}

export default Table
