import React, { useState, useEffect } from 'react'
import Form from './Form'
import Table from './Table'
import axios from 'axios'


const Fruit = () => {
	const fruitData = [];
	const defaultData = {
		nama: "",
		harga: 0,
		berat: 0
	};

	const [fruits, setFruits] = useState(fruitData)
	const [emptyData, setEmptyData] = useState(defaultData)

	useEffect( () => {
		getData()
  }, [])

	const getData = () => {
		let sourceFruits = [];
    axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
    .then(res => {
			for (let i = 0; i < res.data.length; i++){
				let newFruit = {
					id: res.data[i].id,
					nama: res.data[i].name,
					harga: res.data[i].price,
					berat: res.data[i].weight
				}
				sourceFruits.push(newFruit);
			};
    }).finally(() =>{
			setFruits(sourceFruits)
		})
	}

  const add = (fruit) => {
		axios.post(`http://backendexample.sanbercloud.com/api/fruits`,
				{ name: fruit.nama, weight: fruit.berat, price: fruit.harga })
	    .then(res => {
	      getData()
	    })
		setEmptyData(defaultData)
  }

  const updateFruit = (index, fruit) => {
		axios.put(`http://backendexample.sanbercloud.com/api/fruits/` + index,
			{ name: fruit.nama, weight: fruit.berat, price: fruit.harga })
	    .then(res => {
	      getData()
	    })
  }

  const deleteFruit = (index) => {
		axios.delete(`http://backendexample.sanbercloud.com/api/fruits/` + index)
	    .then(res => {
	      getData()
	    })
  }

	return (
    <section id="section-table">
			<h1>Daftar Harga Buah</h1>
      <Form handleAdd={add} fruit={emptyData} />
      <Table
        updateFruit={updateFruit}
        deleteFruit={deleteFruit}
        fruits={fruits} />
    </section>
  )
}

export default Fruit
