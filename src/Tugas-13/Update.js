import React, { useState } from 'react'

const Modal = ({
    handleClose,
    handleSave,
    handleChangeNama,
    handleChangeBerat,
    handleChangeHarga,
    show,
    nama,
    berat,
    harga
}) => {
    const showHideClassName = show ? 'modal display-block' : 'modal display-none';

    return (
      <div id="formModal" className={showHideClassName}>
        <div className="modal-content">
          <div className="modal-header">
            <span onClick={handleClose} className="close"> &times; </span>
            <h2> Update data </h2>
          </div>
          <div className="modal-body">
            <table>
              <tbody>
                <tr>
                  <td> Name </td>
                  <td> <input type="text"
                          onChange={handleChangeNama}
                          defaultValue={nama}/>
                  </td>
                </tr>
                <tr>
                  <td> Harga </td>
                  <td> <input type="text"
                          onChange={handleChangeHarga}
                          defaultValue={harga}/>
                  </td>
                </tr>
                <tr>
                  <td> Berat(g) </td>
                  <td> < input type="text"
                          onChange={handleChangeBerat}
                          defaultValue={berat}/>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="modal-footer">
            <button onClick={handleSave}> save </button>
          </div>
        </div>
      </div>
    );
};

const Update = ({handleUpdate, index, fruit}) => {
  const [show, setShow] = useState(false)
  const [nama, setNama] = useState(fruit.nama)
  const [harga, setHarga] = useState(fruit.harga)
  const [berat, setBerat] = useState(fruit.berat)

  const showModal = () => {
    setShow(true)
  }

  const hideModal = () => {
    setShow(false)
  }

  const submit = () => {
    setShow(false)
    let updatedFruit = {
        nama: nama,
        harga: harga,
        berat: berat
    }
    handleUpdate(index, updatedFruit)
  }

  const handleChangeNama = (event) => {
    setNama(event.target.value)
  }

  const handleChangeBerat = (event) => {
    setBerat(event.target.value)
  }

  const handleChangeHarga = (event) => {
    setHarga(event.target.value)
  }

  return (
    <div>
      <button type='button' onClick={ showModal }> Update < /button>
      <Modal
        show={ show }
        handleClose={ hideModal }
        handleSave={ submit }
        handleChangeNama={ handleChangeNama }
        handleChangeBerat={ handleChangeBerat }
        handleChangeHarga={ handleChangeHarga }
        nama={ fruit.nama }
        berat={ fruit.berat }
        harga={ fruit.harga } />
    </div>
  )
}

export default Update
